CKEDITOR.plugins.add( 'ovidentia', {
    icons: 'ovidentia',
    init: function( editor ) {
    	editor.addCommand(
			'ovidentiaCommand',
			{
				exec: function(t)
				{
					parent.global_editor = {
						'insertHTML': function(text) {
							t.insertHtml(text);
						},
						
	
						'getSelectedHTML': function() {
							// TODO call the correct ckeditor code to get the current selection content
							return '';
						}
					}

					parent.global_uid = t.name;
					parent.insert_ovidentia();
				}
			}
		);
    	editor.ui.addButton( 'Ovidentia', {
    	    label: 'Ovidentia',
    	    command: 'ovidentiaCommand',
    	    toolbar: 'insert'
    	});
		
		CKEDITOR.dialog.add( 'ovidentiaCommand', this.path + 'dialogs/ovidentia.js' );
    }
});