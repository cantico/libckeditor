CKEDITOR.plugins.add( 'ovidentiaimage', {
    icons: 'ovidentiaimage',
    init: function( editor ) {
    	editor.addCommand(
			'ovidentiaImageCommand',
			{
				exec: function(t)
				{
					parent.global_editor = {
						'insertHTML': function(text) {
							t.insertHtml(text);
						},
						
	
						'getSelectedHTML': function() {
							// TODO call the correct ckeditor code to get the current selection content
							return '';
						}
					}

					parent.global_uid = t.name;
					
					var path = document.location.href.split('?')[0];
					
					var useparam = {
						'width'		: 700,
						'height'	: 500
					};

					bab_dialog(path+'?tg=images', useparam, function(arr) {
						eval(arr['callback']+'(arr[\'param\']);');
					});
				}
			}
		);
    	editor.ui.addButton( 'Ovidentiaimage', {
    	    label: 'Image',
    	    command: 'ovidentiaImageCommand',
    	    toolbar: 'insert'
    	});
		
		CKEDITOR.dialog.add( 'ovidentiaImageCommand', this.path + 'dialogs/ovidentiaimage.js' );
    }
});