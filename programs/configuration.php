<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

/**
* Instanciates the widget factory.
*
* @return Func_Widgets
*/
function ckeditor_Widgets()
{
    return bab_Functionality::get('Widgets');
}

function ckeditor_translate($str)
{
    return bab_translate($str, 'LibCkEditor');
}

function ckeditor_iconised($texte, $val = true, $path = '', $showDefault = false){
    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/LibCkEditor'.$path);
    $W = ckeditor_Widgets();

    $defaultClass = "";
    if($showDefault && !$val){
        $defaultClass = 'widget-regexplineedit-invalid';
    }

    $image = $W->Html('
            <span role="presentation" class="cke_ltr cke_toolbar">
                <span role="presentation" class="cke_toolgroup">
                    <span class="cke_button">
                        <a title="'.ckeditor_translate($texte).'" class="cke_button cke_button_off cke_button__'.strtolower($texte).'">
                            <span  class="cke_button_icon cke_button__'.strtolower($texte).'_icon">&nbsp;</span>
                        </a>
                    </span>
                </span>
            </span>');

    return $W->HboxItems(
        $image,
        $temp = $W->Label(ckeditor_translate($texte))->addClass($defaultClass),
        $W->CheckBox()->setAssociatedLabel($temp)->setCheckedValue('1')->setValue($registry->getValue($texte, $val))->setName($texte)
    )->setHorizontalSpacing(5,'px')->setVerticalAlign('middle');
}

function ckeditor_labelised($texte, $value = true, $path = '', $showDefault = false){
    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/LibCkEditor'.$path);
    $W = ckeditor_Widgets();

    $defaultClass = "";
    if($showDefault && !$value){
        $defaultClass = 'widget-regexplineedit-invalid';
    }

    $image = $W->Html('
            <span class="cke_combo cke_combo__format  cke_combo_off">
                <span class="cke_combo_label">'.ckeditor_translate($texte).'</span>
                <a class="cke_combo_button" title="'.ckeditor_translate($texte).'">
                    <span class="cke_combo_text cke_combo_inlinelabel">'.ckeditor_translate($texte).'</span>
                    <span class="cke_combo_open">
                        <span class="cke_combo_arrow"></span>
                    </span>
                </a>
            </span>');

    return $W->HboxItems(
        $image,
        $temp = $W->Label(ckeditor_translate($texte))->addClass($defaultClass),
        $W->CheckBox()->setAssociatedLabel($temp)->setCheckedValue('1')->setValue($registry->getValue($texte, $value))->setName($texte)
    )->setHorizontalSpacing(5,'px')->setVerticalAlign('middle');
}

function ckeditor_config(){
    $addonInfo = bab_getAddonInfosInstance('LibCkEditor');

    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/LibCkEditor');
    //$registry->setKeyValue('name', $workspaceName);

    $W = ckeditor_Widgets();
    $page = $W->BabPage('lcke-page');
    $page->addStyleSheet($addonInfo->getStylePath().'LibCkEditor.css?t=E1PE:5');

    $page->setCurrentItemMenu('setting');

    if(bab_rp('save',false)){
        $page->addError(ckeditor_translate('Record done.'));
    }
    $optionsList = $W->Form(null, $W->VBoxLayout()->setVerticalSpacing(1,'em'))
        ->setHiddenValue('tg', 'addon/LibCkEditor/configuration')
        ->setHiddenValue('idx', 'save')
        ->addItem($W->title(ckeditor_translate('LibCkEditor Settings'),1))
        ->addItem($W->Label(ckeditor_translate('Choose which button should be display').' :'))
        ->addItem(
        $W->HBoxItems(
            $iconColonLeft = $W->VBoxLayout()->setVerticalSpacing(.5,'em'),
            $iconColonRight = $W->VBoxLayout()->setVerticalSpacing(.5,'em')
        )->setHorizontalSpacing(1.5,'em')
    );

    $defaultsLeft = array(
        'Templates' => true,
        'Cut' => true,
        'Copy' => true,
        'Paste' => true,
        'PasteText' => true,
        'PasteFromWord' => true,
        'Undo' => true,
        'Redo' => true,
        'Find' => true,
        'Replace' => true,
        'SpellChecker' => true,
        'SelectAll' => true,
        'RemoveFormat' => true,
        'Maximize' => true,
        'Source' => true,
        'Bold' => true,
        'Italic' => true,
        'Underline' => true,
        'Strike' => true,
        'Subscript' => true,
        'Superscript' => true,
        'JustifyLeft' => true,
        'JustifyCenter' => true,
        'JustifyRight' => true,
        'JustifyBlock' => true
    );

    foreach($defaultsLeft as $k => $v){
        $iconColonLeft->addItem(ckeditor_iconised($k, $v, ''));
    }

    $defaultsRight = array(
        'NumberedList' => true,
        'BulletedList' => true,
        'Outdent' => true,
        'Indent' => true,
        'Table' => true,
        'HorizontalRule' => true,
        'SpecialChar' => true,
        'Link' => true,
        'Unlink' => true,
        'Anchor' => true,
        'Syntaxhighlight' => false,
        'Ovidentia' => true,
        'TextColor' => true,
        'BGColor' => true,
        'Checkbox' => false,
        'Radio' => false,
        'TextField' => false,
        'Textarea' => false,
        'Select' => false,
    );

    foreach($defaultsRight as $k => $v){
        $iconColonRight->addItem(ckeditor_iconised($k, $v, ''));
    }

    $defaultsLabelised = array(
        'Styles' => false,
        'Format' => true,
        'Font' => true,
        'FontSize' => true
    );

    foreach($defaultsLabelised as $k => $v){
        $iconColonRight->addItem(ckeditor_labelised($k, $v, ''));
    }

    $optionsList->addItem($W->SubmitButton()->setLabel(ckeditor_translate('Save')));

    $page->addItem($optionsList);

    $page->displayHtml();
}


function ckeditor_configemail(){
    $addonInfo = bab_getAddonInfosInstance('LibCkEditor');

    $W = ckeditor_Widgets();
    $page = $W->BabPage('lcke-page');

    $I = bab_Functionality::get('Icons');
    if ($I) {
        $I->includeCss();
    }

    $page->addStyleSheet($addonInfo->getRelativePath().'LibCkEditor.css?t=E1PE:5');

    $page->setCurrentItemMenu('email');

    if(bab_rp('save',false)){
        $page->addError(ckeditor_translate('Record done.'));
    }
    $optionsList = $W->Form(null, $W->VBoxLayout()->setVerticalSpacing(1,'em'))
        ->setHiddenValue('tg', 'addon/LibCkEditor/configuration')
        ->setHiddenValue('idx', 'saveEmail')
        ->addItem($W->title(ckeditor_translate('LibCkEditor Settings'),1))
        ->addItem($W->Label(ckeditor_translate('Choose which button should be display for eamil editting').' :'))
        ->addItem(
            $W->Icon(
                ckeditor_translate('Button in red are not fully supported by all mail reader'),
                Func_Icons::STATUS_DIALOG_WARNING
            )->addClass('widget-regexplineedit-invalid')
        )->addClass(Func_Icons::ICON_LEFT_24)
        ->addItem(
        $W->HBoxItems(
            $iconColonLeft = $W->VBoxLayout()->setVerticalSpacing(.5,'em'),
            $iconColonRight = $W->VBoxLayout()->setVerticalSpacing(.5,'em')
        )->setHorizontalSpacing(1.5,'em')
    );

    $defaultsLeft = array(
        'Templates' => false,
        'Cut' => true,
        'Copy' => true,
        'Paste' => true,
        'PasteText' => true,
        'PasteFromWord' => true,
        'Undo' => true,
        'Redo' => true,
        'Find' => true,
        'Replace' => true,
        'SpellChecker' => true,
        'SelectAll' => true,
        'RemoveFormat' => true,
        'Maximize' => true,
        'Source' => true,
        'Bold' => true,
        'Italic' => true,
        'Underline' => true,
        'Strike' => true,
        'Subscript' => false,
        'Superscript' => false,
        'JustifyLeft' => true,
        'JustifyCenter' => true,
        'JustifyRight' => true,
        'JustifyBlock' => true
    );

    foreach($defaultsLeft as $k => $v){
        $iconColonLeft->addItem(ckeditor_iconised($k, $v, '/email', true));
    }

    $defaultsRight = array(
        'NumberedList' => false,
        'BulletedList' => false,
        'Outdent' => false,
        'Indent' => false,
        'Table' => false,
        'HorizontalRule' => false,
        'SpecialChar' => false,
        'Link' => true,
        'Unlink' => true,
        'Anchor' => false,
        'Syntaxhighlight' => false,
        'Ovidentia' => false,
        'TextColor' => true,
        'BGColor' => true,
        'Checkbox' => false,
        'Radio' => false,
        'TextField' => false,
        'Textarea' => false,
        'Select' => false,
        'Ovidentiaimage' => true
    );

    foreach($defaultsRight as $k => $v){
        $iconColonRight->addItem(ckeditor_iconised($k, $v, '/email', true));
    }

    $defaultsLabelised = array(
        'Styles' => false,
        'Format' => false,
        'Font' => true,
        'FontSize' => true,
    );

    foreach($defaultsLabelised as $k => $v){
        $iconColonRight->addItem(ckeditor_labelised($k, $v, '/email', true));
    }

    $optionsList->addItem($W->SubmitButton()->setLabel(ckeditor_translate('Save')));

    $page->addItem($optionsList);

    $page->displayHtml();
}

function ckeditor_styles(){

    $I = bab_Functionality::get('Icons');
    if ($I) {
        $I->includeCss();
    }
    $addonInfo = bab_getAddonInfosInstance('LibCkEditor');
    global $babDB;

    $W = ckeditor_Widgets();
    $page = $W->BabPage('lcke-page');
    $page->addStyleSheet($addonInfo->getRelativePath().'LibCkEditor.css?t=B0VI4XQ');

    $page->setCurrentItemMenu('styles');

    if(bab_rp('save',false) == 1){
        $page->addError(ckeditor_translate('Record done.'));
    }
    if(bab_rp('save',false) == 2){
        $page->addError(ckeditor_translate('Error, a field is missing.'));
    }
    $optionsList = $W->Form(null, $W->VBoxLayout()->setVerticalSpacing(1,'em'))
        ->setHiddenValue('tg', 'addon/LibCkEditor/configuration')
        ->setHiddenValue('idx', 'savestyles')
        ->addItem($W->title(ckeditor_translate('Styles Settings'),1));

    $table = $W->TableView();
    $table->setView(Widget_TableView::VIEW_LIST);
    $table->addSection('lcke-table-header');
    $table->setCurrentSection('lcke-table-header');
    $table->addItem($W->Label(ckeditor_translate('Name')),0,0);
    $table->addItem($W->Label(ckeditor_translate('Class')),0,1);
    $table->addItem($W->Label(''),0,2);
    $table->addSection('lcke-table-content');
    $table->setCurrentSection('lcke-table-content');

    $req = "SELECT * FROM lcke_styles";
    $res = $babDB->db_query($req);

    $x = 1;
    while($arr = $babDB->db_fetch_assoc($res)){
        $y = 0;
        $table->addItem(
            $W->label($arr['name']),
            $x,
            $y
        );
        $y++;
        $table->addItem(
            $W->label($arr['class']),
            $x,
            $y
        );
        $y++;
        $table->addItem(
            $W->HBoxItems(
                $W->Link(
                    $W->Icon('',Func_Icons::ACTIONS_EDIT_DELETE),
                    $GLOBALS['babAddonUrl'].'configuration&idx=deletestyles&id='.$arr['id']
                )->setConfirmationMessage(ckeditor_translate('This will delete this style, continue?'))->setTitle(ckeditor_translate('Delete'))/*,
                $linkNext*/
            )->addClass('icon-left-24')->addClass('icon-24x24')->addClass('icon-left'),
            $x,
            $y
        );
        $x++;
    }

    $optionsList->addItem($table);

    $optionsList->addItem(
        $W->FlowItems(
            $W->VBoxItems(
                $tmp = $W->Label(ckeditor_translate('Name'))->colon(true),
                $W->LineEdit()->setName('name')->setAssociatedLabel($tmp)
            )->setVerticalSpacing(0.25,'em'),
            $W->VBoxItems(
                $tmp = $W->Label(ckeditor_translate('Class'))->colon(true),
                $W->LineEdit()->setName('class')->setAssociatedLabel($tmp)
            )->setVerticalSpacing(0.25,'em')
        )->setHorizontalSpacing(2,'em')->setVerticalSpacing(1,'em')
    );

    $optionsList->addItem($W->SubmitButton()->validate(true)->setLabel(ckeditor_translate('Add')));

    $page->addItem($optionsList);

    $page->displayHtml();
}

function ckeditor_deletestyles(){
    global $babDB;

    $id = bab_gp('id','');

    if(!$id){
        header('Location: ' . '?tg=addon/LibCkEditor/configuration&idx=styles&save=2');
        die;
    }

    $req = "DELETE FROM lcke_styles WHERE id=".$babDB->quote($id);
    $babDB->db_query($req);

    ckeditor_genstyles();

    header('Location: ' . '?tg=addon/LibCkEditor/configuration&idx=styles&save=1');
}

function ckeditor_savestyles(){
    global $babDB;

    $name = bab_pp('name','');
    $class = bab_pp('class','');

    if(!$name || !$class){
        header('Location: ' . '?tg=addon/LibCkEditor/configuration&idx=styles&save=2');
        die;
    }

    $req = "INSERT INTO lcke_styles (name, class) VALUES (".$babDB->quote($name).",".$babDB->quote($class).")";
    $babDB->db_query($req);

    ckeditor_genstyles();

    header('Location: ' . '?tg=addon/LibCkEditor/configuration&idx=styles&save=1');
}

function ckeditor_genstyles(){
    global $babDB;

    $req = "SELECT * FROM lcke_styles";
    $res = $babDB->db_query($req);

    $addonInfo = bab_getAddonInfosInstance('LibCkEditor');

    $uPath = new bab_Path($addonInfo->getUploadPath());
    if($uPath->isDir()){
        $uPath->push('styles.js');
        if(is_file($uPath->tostring())){
            $ourFileHandle = fopen($uPath->tostring(), 'w+');
            if(!$ourFileHandle){
                return -1;
            }
            fwrite($ourFileHandle, "CKEDITOR.stylesSet.add( 'my_styles', [");
        }
    }

    $notfirst = false;
    while($arr = $babDB->db_fetch_assoc($res)){
        if($notfirst){
            fwrite($ourFileHandle, ", ");
        }else{
            $notfirst = true;
        }
        fwrite($ourFileHandle, "{ name : '".$arr['name']."', element : 'p', attributes : { 'class' : '".$arr['class']."' } }");
    }

    fwrite($ourFileHandle, "]);");
    fclose($ourFileHandle);
}

function ckeditor_save(){
    $addonInfo = bab_getAddonInfosInstance('LibCkEditor');

    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/LibCkEditor');

    $registry->setKeyValue('Templates', bab_pp('Templates', true));

    $registry->setKeyValue('Cut', bab_pp('Cut', true));
    $registry->setKeyValue('Copy', bab_pp('Copy', true));
    $registry->setKeyValue('Paste', bab_pp('Paste', true));
    $registry->setKeyValue('PasteText', bab_pp('PasteText', true));
    $registry->setKeyValue('PasteFromWord', bab_pp('PasteFromWord', true));

    $registry->setKeyValue('Undo', bab_pp('Undo', true));
    $registry->setKeyValue('Redo', bab_pp('Redo', true));

    $registry->setKeyValue('Find', bab_pp('Find', true));
    $registry->setKeyValue('Replace', bab_pp('Replace', true));
    $registry->setKeyValue('SpellChecker', bab_pp('SpellChecker', true));

    $registry->setKeyValue('SelectAll', bab_pp('SelectAll', true));
    $registry->setKeyValue('RemoveFormat', bab_pp('RemoveFormat', true));

    $registry->setKeyValue('Maximize', bab_pp('Maximize', true));
    $registry->setKeyValue('Source', bab_pp('Source', true));

    $registry->setKeyValue('Bold', bab_pp('Bold', true));
    $registry->setKeyValue('Italic', bab_pp('Italic', true));
    $registry->setKeyValue('Underline', bab_pp('Underline', true));
    $registry->setKeyValue('Strike', bab_pp('Strike', true));
    $registry->setKeyValue('Subscript', bab_pp('Subscript', true));
    $registry->setKeyValue('Superscript', bab_pp('Superscript', true));

    $registry->setKeyValue('JustifyLeft', bab_pp('JustifyLeft', true));
    $registry->setKeyValue('JustifyCenter', bab_pp('JustifyCenter', true));
    $registry->setKeyValue('JustifyRight', bab_pp('JustifyRight', true));
    $registry->setKeyValue('JustifyBlock', bab_pp('JustifyBlock', true));

    $registry->setKeyValue('NumberedList', bab_pp('NumberedList', true));
    $registry->setKeyValue('BulletedList', bab_pp('BulletedList', true));

    $registry->setKeyValue('Outdent', bab_pp('Outdent', true));
    $registry->setKeyValue('Indent', bab_pp('Indent', true));

    $registry->setKeyValue('Table', bab_pp('Table', true));
    $registry->setKeyValue('HorizontalRule', bab_pp('HorizontalRule', true));
    $registry->setKeyValue('SpecialChar', bab_pp('SpecialChar', true));

    $registry->setKeyValue('Link', bab_pp('Link', true));
    $registry->setKeyValue('Unlink', bab_pp('Unlink', true));
    $registry->setKeyValue('Anchor', bab_pp('Anchor', true));
    $registry->setKeyValue('Syntaxhighlight', bab_pp('Syntaxhighlight', false));
    $registry->setKeyValue('Ovidentia', bab_pp('Ovidentia', true));

    $registry->setKeyValue('Styles', bab_pp('Styles', false));
    $registry->setKeyValue('Format', bab_pp('Format', true));
    $registry->setKeyValue('Font', bab_pp('Font', true));
    $registry->setKeyValue('fontSize', bab_pp('fontSize', true));

    $registry->setKeyValue('TextColor', bab_pp('TextColor', true));
    $registry->setKeyValue('BgColor', bab_pp('BgColor', true));

    $registry->setKeyValue('Checkbox', bab_pp('Checkbox', false));
    $registry->setKeyValue('Radio', bab_pp('Radio', false));
    $registry->setKeyValue('TextField', bab_pp('Radio', false));
    $registry->setKeyValue('Textarea', bab_pp('Radio', false));
    $registry->setKeyValue('Select', bab_pp('Select', false));

    $registry->setKeyValue('Ovidentiaimage', false);

    header('Location: ' . '?tg=addon/LibCkEditor/configuration&idx=config&save=1');
}

function ckeditor_saveEmail(){
    $addonInfo = bab_getAddonInfosInstance('LibCkEditor');

    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/LibCkEditor/email');

    $registry->setKeyValue('Templates', bab_pp('Templates', true));

    $registry->setKeyValue('Cut', bab_pp('Cut', true));
    $registry->setKeyValue('Copy', bab_pp('Copy', true));
    $registry->setKeyValue('Paste', bab_pp('Paste', true));
    $registry->setKeyValue('PasteText', bab_pp('PasteText', true));
    $registry->setKeyValue('PasteFromWord', bab_pp('PasteFromWord', true));

    $registry->setKeyValue('Undo', bab_pp('Undo', true));
    $registry->setKeyValue('Redo', bab_pp('Redo', true));

    $registry->setKeyValue('Find', bab_pp('Find', true));
    $registry->setKeyValue('Replace', bab_pp('Replace', true));
    $registry->setKeyValue('SpellChecker', bab_pp('SpellChecker', true));

    $registry->setKeyValue('SelectAll', bab_pp('SelectAll', true));
    $registry->setKeyValue('RemoveFormat', bab_pp('RemoveFormat', true));

    $registry->setKeyValue('Maximize', bab_pp('Maximize', true));
    $registry->setKeyValue('Source', bab_pp('Source', true));

    $registry->setKeyValue('Bold', bab_pp('Bold', true));
    $registry->setKeyValue('Italic', bab_pp('Italic', true));
    $registry->setKeyValue('Underline', bab_pp('Underline', true));
    $registry->setKeyValue('Strike', bab_pp('Strike', true));
    $registry->setKeyValue('Subscript', bab_pp('Subscript', true));
    $registry->setKeyValue('Superscript', bab_pp('Superscript', true));

    $registry->setKeyValue('JustifyLeft', bab_pp('JustifyLeft', true));
    $registry->setKeyValue('JustifyCenter', bab_pp('JustifyCenter', true));
    $registry->setKeyValue('JustifyRight', bab_pp('JustifyRight', true));
    $registry->setKeyValue('JustifyBlock', bab_pp('JustifyBlock', true));

    $registry->setKeyValue('NumberedList', bab_pp('NumberedList', true));
    $registry->setKeyValue('BulletedList', bab_pp('BulletedList', true));

    $registry->setKeyValue('Outdent', bab_pp('Outdent', true));
    $registry->setKeyValue('Indent', bab_pp('Indent', true));

    $registry->setKeyValue('Table', bab_pp('Table', true));
    $registry->setKeyValue('HorizontalRule', bab_pp('HorizontalRule', true));
    $registry->setKeyValue('SpecialChar', bab_pp('SpecialChar', true));

    $registry->setKeyValue('Link', bab_pp('Link', true));
    $registry->setKeyValue('Unlink', bab_pp('Unlink', true));
    $registry->setKeyValue('Anchor', bab_pp('Anchor', true));
    $registry->setKeyValue('Syntaxhighlight', bab_pp('Syntaxhighlight', false));
    $registry->setKeyValue('Ovidentia', bab_pp('Ovidentia', true));

    $registry->setKeyValue('Styles', bab_pp('Styles', false));
    $registry->setKeyValue('Format', bab_pp('Format', true));
    $registry->setKeyValue('Font', bab_pp('Font', true));
    $registry->setKeyValue('fontSize', bab_pp('fontSize', true));

    $registry->setKeyValue('TextColor', bab_pp('TextColor', true));
    $registry->setKeyValue('BgColor', bab_pp('BgColor', true));

    $registry->setKeyValue('Checkbox', bab_pp('Checkbox', false));
    $registry->setKeyValue('Radio', bab_pp('Radio', false));
    $registry->setKeyValue('TextField', bab_pp('Radio', false));
    $registry->setKeyValue('Textarea', bab_pp('Radio', false));
    $registry->setKeyValue('Select', bab_pp('Select', false));

    $registry->setKeyValue('Ovidentiaimage', bab_pp('Ovidentiaimage', false));

    header('Location: ' . '?tg=addon/LibCkEditor/configuration&idx=email&save=1');
}

$idx = bab_rp('idx','config');

$page = $GLOBALS['babBody'];

$page->addItemMenu('lib', ckeditor_translate('Shared Libraries'), '?tg=addons&idx=library');
$page->addItemMenu('setting', ckeditor_translate('Classic'), '?tg=addon/LibCkEditor/configuration&idx=config');
$page->addItemMenu('email', ckeditor_translate('Email'), '?tg=addon/LibCkEditor/configuration&idx=email');
$page->addItemMenu('styles', ckeditor_translate('Add Styles'), '?tg=addon/LibCkEditor/configuration&idx=styles');

switch($idx){
    case 'save':
        ckeditor_save();
        break;
    case 'saveEmail':
        ckeditor_saveEmail();
        break;
    case 'styles':
        ckeditor_styles();
        break;
    case 'savestyles':
        ckeditor_savestyles();
        break;
    case 'deletestyles':
        ckeditor_deletestyles();
        break;
    case 'email':
        ckeditor_configemail();
        break;
    case 'config':
    default:
        ckeditor_config();
        break;
}