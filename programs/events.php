<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';

/**
 * Ceci est une fonction evenementielle enregistree avec bab_addEventListener
 * elle sera appelee lors de l'affichage d'une zone de texte editeur WYSIWYG
 *
 * @param	bab_eventEditorContentToEditor	&$event
 *
 */
function ckeditor_onContentToEditor(bab_eventEditorContentToEditor $event) {
    if (!empty($event->content) && 'html' !== $event->format && 'email' !== $event->format) {
        /**
         * Pour le moment seul le format html est géré par le noyau mais il faut ignorer les autres format s'ils ne sont pas supportés
         * car d'autres formats pourrais étres utilisés à l'avenir, gérés par leurs propres modules
         */
        return;
    }

	$addon = bab_getAddonInfosInstance('LibCkEditor');

    require_once $addon->getPhpPath()."ckeditor/ckeditor.php";

    $obj = new ckeditor($addon->getPhpPath().'ckeditor/');// le chemin d'installation de l'add-on

    if( empty($event->content)){
        $content = "";
    }else{
        $content = preg_replace("/src=\"(images[^\"]+)\"/", "src=\"".$GLOBALS['babUrl']."\\1\"", $event->content);
    }
    $obj->returnOutput = true;

    $obj->config['height'] = $event->parameters['height'];

    if($event->format == 'email'){
        $toolbar = ckeditor_getEmailToolbar();
    }else{
        $toolbar = ckeditor_getToolbar();
    }

    $config['toolbar'] = $toolbar;
    //$config['forcePasteAsPlainText'] = true;
    //$config['pasteFromWordRemoveStyles'] = true;
    //$config['pasteFromWordRemoveFontStyles'] = true;

    $config['language'] = 'fr';

    $config['extraPlugins'] = 'ovidentia,ovidentiaimage,syntaxhighlight';

    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/LibCkEditor');
    if($registry->getValue('Styles', false)){
        $addonInfo = bab_getAddonInfosInstance('LibCkEditor');
        $uPath = new bab_Path($addonInfo->getUploadPath());
        $uPath->push('styles.js');
        if(is_file($uPath->tostring())){
            $config['stylesSet'] = 'my_styles:'.$GLOBALS['babUrl'].'index.php?tg=addon/LibCkEditor/styles';
        }
    }

    $html = '<input type="hidden" name="'.$event->fieldname.'_format" value="html" />';
    $obj->textareaAttributes['id'] = $event->uid;
    $html .= $obj->editor($event->fieldname, $content, $config);

    $event->setOutputEditor($html);
}

function ckeditor_getGeneralToolbar($registry, $default) {
    $return = array();

    if($registry->getValue('Templates', $default['Templates'])){
        $return[] = array('Templates');
    }

    $temp = array();
    if($registry->getValue('Cut', $default['Cut'])){
        $temp[] = 'Cut';
    }
    if($registry->getValue('Copy', $default['Copy'])){
        $temp[] = 'Copy';
    }
    if($registry->getValue('Paste', $default['Paste'])){
        $temp[] = 'Paste';
    }
    if($registry->getValue('PasteText', $default['PasteText'])){
        $temp[] = 'PasteText';
    }
    if($registry->getValue('PasteFromWord', $default['PasteFromWord'])){
        $temp[] = 'PasteFromWord';
    }
    if(!empty($temp)){
        $return[] = $temp;
    }

    $temp = array();
    if($registry->getValue('Undo', $default['Undo'])){
        $temp[] = 'Undo';
    }
    if($registry->getValue('Redo', $default['Redo'])){
        $temp[] = 'Redo';
    }
    if(!empty($temp)){
        $return[] = $temp;
    }

    $temp = array();
    if($registry->getValue('Find', $default['Find'])){
        $temp[] = 'Find';
    }
    if($registry->getValue('Replace', $default['Replace'])){
        $temp[] = 'Replace';
    }
    if($registry->getValue('SpellChecker', $default['SpellChecker'])){
        $temp[] = 'Scayt';
    }
    if(!empty($temp)){
        $return[] = $temp;
    }

    $temp = array();
    if($registry->getValue('SelectAll', $default['SelectAll'])){
        $temp[] = 'SelectAll';
    }
    if($registry->getValue('RemoveFormat', $default['RemoveFormat'])){
        $temp[] = 'RemoveFormat';
    }
    if(!empty($temp)){
        $return[] = $temp;
    }

    $temp = array();
    if($registry->getValue('Maximize', $default['Maximize'])){
        $temp[] = 'Maximize';
    }
    if($registry->getValue('Source', $default['Source'])){
        $temp[] = 'Source';
    }
    if(!empty($temp)){
        $return[] = $temp;
    }

    $temp = array();
    if($registry->getValue('Bold', $default['Bold'])){
        $temp[] = 'Bold';
    }
    if($registry->getValue('Italic', $default['Italic'])){
        $temp[] = 'Italic';
    }
    if($registry->getValue('Underline', $default['Underline'])){
        $temp[] = 'Underline';
    }
    if($registry->getValue('Strike', $default['Strike'])){
        $temp[] = 'Strike';
    }
    if($registry->getValue('Subscript', $default['Subscript'])){
        $temp[] = 'Subscript';
    }
    if($registry->getValue('Superscript', $default['Superscript'])){
        $temp[] = 'Superscript';
    }
    if(!empty($temp)){
        $return[] = $temp;
    }

    $temp = array();
    if($registry->getValue('JustifyLeft', $default['JustifyLeft'])){
        $temp[] = 'JustifyLeft';
    }
    if($registry->getValue('JustifyCenter', $default['JustifyCenter'])){
        $temp[] = 'JustifyCenter';
    }
    if($registry->getValue('JustifyRight', $default['JustifyRight'])){
        $temp[] = 'JustifyRight';
    }
    if($registry->getValue('JustifyBlock', $default['JustifyBlock'])){
        $temp[] = 'JustifyBlock';
    }
    if(!empty($temp)){
        $return[] = $temp;
    }

    $temp = array();
    if($registry->getValue('NumberedList', $default['NumberedList'])){
        $temp[] = 'NumberedList';
    }
    if($registry->getValue('BulletedList', $default['BulletedList'])){
        $temp[] = 'BulletedList';
    }
    if(!empty($temp)){
        $return[] = $temp;
    }

    $temp = array();
    if($registry->getValue('Outdent', $default['Outdent'])){
        $temp[] = 'Outdent';
    }
    if($registry->getValue('Indent', $default['Indent'])){
        $temp[] = 'Indent';
    }
    if(!empty($temp)){
        $return[] = $temp;
    }

    $temp = array();
    if($registry->getValue('Table', $default['Table'])){
        $temp[] = 'Table';
    }
    if($registry->getValue('HorizontalRule', $default['HorizontalRule'])){
        $temp[] = 'HorizontalRule';
    }
    if($registry->getValue('SpecialChar', $default['SpecialChar'])){
        $temp[] = 'SpecialChar';
    }
    if(!empty($temp)){
        $return[] = $temp;
    }

    $temp = array();
    if($registry->getValue('Link', $default['Link'])){
        $temp[] = 'Link';
    }
    if($registry->getValue('Unlink', $default['Unlink'])){
        $temp[] = 'Unlink';
    }
    if($registry->getValue('Anchor', $default['Anchor'])){
        $temp[] = 'Anchor';
    }
    if($registry->getValue('Syntaxhighlight', $default['Syntaxhighlight'])){
        $temp[] = 'Syntaxhighlight';
    }
    if($registry->getValue('Ovidentia', $default['Ovidentia'])){
        $temp[] = 'Ovidentia';
    }
    if(!empty($temp)){
        $return[] = $temp;
    }

    $temp = array();
    if($registry->getValue('Styles', $default['Styles'])){
        $temp[] = 'Styles';
    }
    if($registry->getValue('Format', $default['Format'])){
        $temp[] = 'Format';
    }
    if($registry->getValue('Font', $default['Font'])){
        $temp[] = 'Font';
    }
    if($registry->getValue('FontSize', $default['FontSize'])){
        $temp[] = 'FontSize';
    }
    if(!empty($temp)){
        $return[] = $temp;
    }

    $temp = array();
    if($registry->getValue('TextColor', $default['TextColor'])){
        $temp[] = 'TextColor';
    }
    if($registry->getValue('BGColor', $default['BGColor'])){
        $temp[] = 'BGColor';
    }
    if(!empty($temp)){
        $return[] = $temp;
    }

    $temp=array();
    if($registry->getValue('Checkbox', $default['Checkbox'])){
        $temp[] = 'Checkbox';
    }
    if($registry->getValue('Radio', $default['Radio'])){
        $temp[] = 'Radio';
    }
    if($registry->getValue('TextField', $default['TextField'])){
        $temp[] = 'TextField';
    }
    if($registry->getValue('Textarea', $default['Textarea'])){
        $temp[] = 'Textarea';
    }
    if($registry->getValue('Select', $default['Select'])){
        $temp[] = 'Select';
    }
     if($registry->getValue('Ovidentiaimage', $default['Ovidentiaimage'])){
         $temp[] = 'Ovidentiaimage';
     }
    if(!empty($temp)){
        $return[] = $temp;
    }

    return $return;
}

function ckeditor_getEmailToolbar() {
    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/LibCkEditor/email');

    $default = array(
        'Templates' => false,
        'Cut' => true,
        'Copy' => true,
        'Paste' => true,
        'PasteText' => true,
        'PasteFromWord' => true,
        'Undo' => true,
        'Redo' => true,
        'Find' => true,
        'Replace' => true,
        'SpellChecker' => true,
        'SelectAll' => true,
        'RemoveFormat' => true,
        'Maximize' => true,
        'Source' => true,
        'Bold' => true,
        'Italic' => true,
        'Underline' => true,
        'Strike' => true,
        'Subscript' => false,
        'Superscript' => false,
        'JustifyLeft' => true,
        'JustifyCenter' => true,
        'JustifyRight' => true,
        'JustifyBlock' => true,
        'NumberedList' => false,
        'BulletedList' => false,
        'Outdent' => false,
        'Indent' => false,
        'Table' => false,
        'HorizontalRule' => false,
        'SpecialChar' => false,
        'Link' => true,
        'Unlink' => true,
        'Anchor' => false,
        'Syntaxhighlight' => false,
        'Ovidentia' => false,
        'Styles' => false,
        'Format' => false,
        'Font' => true,
        'FontSize' => true,
        'TextColor' => true,
        'BGColor' => true,
        'Checkbox' => false,
        'Radio' => false,
        'TextField' => false,
        'Textarea' => false,
        'Select' => false,
        'Ovidentiaimage' => true
    );

    return ckeditor_getGeneralToolbar($registry, $default);
}


function ckeditor_getToolbar() {
    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/LibCkEditor');

    if($registry->getValue('Templates', true)){
        $return[] = array('Templates');
    }

    $default = array(
        'Templates' => true,
        'Cut' => true,
        'Copy' => true,
        'Paste' => true,
        'PasteText' => true,
        'PasteFromWord' => true,
        'Undo' => true,
        'Redo' => true,
        'Find' => true,
        'Replace' => true,
        'SpellChecker' => true,
        'SelectAll' => true,
        'RemoveFormat' => true,
        'Maximize' => true,
        'Source' => true,
        'Bold' => true,
        'Italic' => true,
        'Underline' => true,
        'Strike' => true,
        'Subscript' => true,
        'Superscript' => true,
        'JustifyLeft' => true,
        'JustifyCenter' => true,
        'JustifyRight' => true,
        'JustifyBlock' => true,
        'NumberedList' => true,
        'BulletedList' => true,
        'Outdent' => true,
        'Indent' => true,
        'Table' => true,
        'HorizontalRule' => true,
        'SpecialChar' => true,
        'Link' => true,
        'Unlink' => true,
        'Anchor' => true,
        'Syntaxhighlight' => false,
        'Ovidentia' => true,
        'Styles' => false,
        'Format' => true,
        'Font' => true,
        'FontSize' => true,
        'TextColor' => true,
        'BGColor' => true,
        'Checkbox' => false,
        'Radio' => false,
        'TextField' => false,
        'Textarea' => false,
        'Select' => false,
        'Ovidentiaimage' => false
    );

    return ckeditor_getGeneralToolbar($registry, $default);
}


/**
 * Ceci est une fonction evenementielle enregistree avec bab_addEventListener
 * elle sera appelee lorsque le formulaire est soumis et que le programme cherche a enregistrer le texte saisi dans la base
 *
 * @param	bab_eventEditorRequestToContent	&$event
 *
 */
function ckeditor_onRequestToContent(bab_eventEditorRequestToContent $event) {

    if ('html' !== bab_pp($event->fieldname.'_format')) {
        return;
    }

    $content = bab_pp($event->fieldname);
    bab_editor_record($content);

    $event->setOutputContent($content, 'html');
}

/**
 * Ceci est une fonction evenementielle enregistree avec bab_addEventListener
 * elle sera appelee lorsque du texte saisi par l'editeur doit etre affiche en html
 *
 * @param	bab_eventEditorContentToHtml	&$event
 *
 */
function ckeditor_onContentToHtml(bab_eventEditorContentToHtml $event) {


    if ('html' !== $event->format) {
        return;
    }

    if (null !== $event->output_html) {
        // allredy done by another editor
        return;
    }

    if ('' === $event->content)
    {
        $event->setOutputHtml('');
        return;
    }

    $event->setOutputHtml(bab_toHtml($event->content, BAB_HTML_REPLACE));
}

?>
